name = homework5

$(name).pdf : $(name).tex $(name).bib problem1.tex problem2.tex problem3.tex problem4.tex problem5.tex problem6.tex
	pdflatex $(name) && \
	bibtex   $(name) && \
	pdflatex $(name) && \
	pdflatex $(name)

clean :
	rm -f \
	$(name).aux \
	$(name).bbl \
	$(name).blg \
	$(name).log \
	$(name).out \
	$(name).pdf
