In this problem, we trace through a series of virtual memory references. The memory configuration uses $4\unit{KB}$ pages, a flat page table, and includes a 4-entry fully-associative TLB with true LRU replacement. We assume memory is byte-addressable.

When a memory reference results in a page fault, the page moved into memory will be assigned the smallest available physical page number (PPN) beginning with 1.

For notational convenience, we track LRU information in the TLB using a mutable sequence of 4 numbers. At any given time, the least-recently-used TLB entry is indicated by the smallest number in the LRU column. To mark an entry as used, we assign a new LRU number to it that is larger than the other LRU numbers. 

The initial state of the TLB and page table is given below.
\begin{center}
\begin{tabular}{|r|r|r|r|}
\multicolumn{4}{c}{TLB} \\
\hline
Valid & VPN & PPN & LRU \\
\hline
1 & 11 & 12 & 1 \\
1 & 7  & 4  & 2 \\
1 & 3  & 6  & 3 \\
0 & 4  & 9  & 0 \\
\hline
\end{tabular}
\hspace{2em}
\begin{tabular}{|r|r|r|}
\multicolumn{3}{c}{Page table} \\
\hline
Index & Valid & PPN \\
\hline
0  & 1 & 5    \\
1  & 0 & Disk \\
2  & 0 & Disk \\
3  & 1 & 6    \\
4  & 1 & 9    \\
5  & 1 & 11   \\
6  & 0 & Disk \\
7  & 1 & 4    \\
8  & 0 & Disk \\
9  & 0 & Disk \\
10 & 1 & 3    \\
11 & 1 & 12   \\
12 & 0 & Disk \\
\hline
\end{tabular}
\end{center}

We will trace the following sequence of memory references. For each, we divide by the page size ($4\unit{KB} = 4096\unit{B}$) to calculate the virtual page number (VPN) and then use the TLB and page table to determine how the system will handle the reference.
\[ 4669, 2227, 13916, 34587, 48870, 12608, 49225 \]

The first four memory references are presented in the table below.
\begin{center}
\begin{tabular}{|r|r|l|r|}
\hline
Address & VPN & What happens? & PPN \\
\hline
4669  & 1  & Page fault               & 1 \\
2227  & 0  & TLB miss, page table hit & 5 \\
13916 & 3  & TLB hit                  & 6 \\
34587 & 8  & Page fault               & 2 \\
\hline
\end{tabular}
\end{center}
At this point, the full state of the TLB and page table is:
\begin{center}
\begin{tabular}{|r|r|r|r|}
\multicolumn{4}{c}{TLB} \\
\hline
Valid & VPN & PPN & LRU \\
\hline
1 & 0  & 5  & 5 \\
1 & 8  & 2  & 7 \\
1 & 3  & 6  & 6 \\
1 & 1  & 1  & 4 \\
\hline
\end{tabular}
\hspace{2em}
\begin{tabular}{|r|r|r|}
\multicolumn{3}{c}{Page table} \\
\hline
Index & Valid & PPN \\
\hline
0  & 1 & 5    \\
1  & 1 & 1    \\
2  & 0 & Disk \\
3  & 1 & 6    \\
4  & 1 & 9    \\
5  & 1 & 11   \\
6  & 0 & Disk \\
7  & 1 & 4    \\
8  & 1 & 2    \\
9  & 0 & Disk \\
10 & 1 & 3    \\
11 & 1 & 12   \\
12 & 0 & Disk \\
\hline
\end{tabular}
\end{center}

The next three memory references are presented below.
\begin{center}
\begin{tabular}{|r|r|l|r|}
\hline
Address & VPN & What happens? & PPN \\
\hline
48870 & 11 & TLB miss, page table hit & 12 \\
12608 & 3  & TLB hit                  & 6  \\
49225 & 12 & Page fault               & 7  \\
\hline
\end{tabular}
\end{center}
The final state of the TLB and page table is:
\begin{center}
\begin{tabular}{|r|r|r|r|}
\multicolumn{4}{c}{TLB} \\
\hline
Valid & VPN & PPN & LRU \\
\hline
1 & 12 & 7  & 10 \\
1 & 8  & 2  & 7 \\
1 & 3  & 6  & 9 \\
1 & 11 & 12 & 8 \\
\hline
\end{tabular}
\hspace{2em}
\begin{tabular}{|r|r|r|}
\multicolumn{3}{c}{Page table} \\
\hline
Index & Valid & PPN \\
\hline
0  & 1 & 5    \\
1  & 1 & 1    \\
2  & 0 & Disk \\
3  & 1 & 6    \\
4  & 1 & 9    \\
5  & 1 & 11   \\
6  & 0 & Disk \\
7  & 1 & 4    \\
8  & 1 & 2    \\
9  & 0 & Disk \\
10 & 1 & 3    \\
11 & 1 & 12   \\
12 & 1 & 7    \\
\hline
\end{tabular}
\end{center}
